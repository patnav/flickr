require "flickraw"

class ApplicationController < ActionController::Base
  protect_from_forgery
  
  before_filter :setup
  
  def index
    if params[:keyword].blank?
      @photos = nil
    else
      @photos = flickr.photos.search(:tags => params[:keyword], :per_page => 10)
    end
  end
  
  protected
    def setup
      FlickRaw.api_key = "59387ca6e53cff9a7418895348f49bd1"
      FlickRaw.shared_secret = "edcd6786b8b132e0"
    end
end
